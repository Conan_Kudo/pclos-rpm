#rpm 4.12.0.2 for PCLinuxOS 2017

# Note: Synchronized with Mageia and Fedora!

%define lib64arches	x86_64 ppc64 sparc64
%define lib64oses	linux

%ifarch %lib64arches
%ifos %lib64oses
    %define _lib lib64
%else
    %define _lib lib
%endif
%else
    %define _lib lib
%endif

%define _prefix /usr
%define _libdir %_prefix/%_lib
%define _bindir %_prefix/bin
%define _sysconfdir /etc
%define _datadir /usr/share
%define _defaultdocdir %_datadir/doc
%define _localstatedir /var
%define _infodir %_datadir/info

# Support for newer Python macros on systems with no support
%{?!python2_sitelib:%define python2_sitelib %{python_sitelib}}
%{?!python2_sitearch:%define python2_sitearch %{python_sitearch}}
%{?!__python2:%define __python2 %{__python}}


%if %{?apply_patches:0}%{?!apply_patches:1}
%define apply_patches %(for p in `grep '^Patch.*:' "%{_specdir}/pclos-rpm.spec" | cut -d':' -f2-`; do echo "patch -p1 -F0 -i %{_sourcedir}/$p"; done )
%endif

# Define directory which holds rpm config files, and some binaries actually
# NOTE: it remains */lib even on lib64 platforms as only one version
#       of rpm is supported anyway, per architecture
%define rpmdir %{_prefix}/lib/rpm

%if %{?mklibname:0}%{?!mklibname:1}
%define mklibname(ds)  %{_lib}%{1}%{?2:%{2}}%{?3:_%{3}}%{-s:-static}%{-d:-devel}
%endif

%if %{?distsuffix:0}%{?!distsuffix:1}
%define distsuffix pclos
%endif

#define subrel 0
%if %{?mkrel:0}%{?!mkrel:1}
%define mkrel(c:) %{-c: 0.%{-c*}.}%{1}%{?distsuffix:%distsuffix}%{?!distsuffix:pclos}%{?pclinuxos_release:%pclinuxos_release}%{?subrel:.%subrel}
%endif

%if %{?mips:0}%{?!mips:1}
%define mips		mips mipsel mips32 mips32el mips64 mips64el
%endif

# PCLinuxOS defines "real vendor" as mandriva
%define _real_vendor mandriva

%if %{?pyver:0}%{?!pyver:1}
%define pyver %(python -V 2>&1 | cut -f2 -d" " | cut -f1,2 -d".")
%endif

%define __find_requires %{rpmdir}/%{_real_vendor}/find-requires %{?buildroot:%{buildroot}} %{?_target_cpu:%{_target_cpu}}
%define __find_provides %{rpmdir}/%{_real_vendor}/find-provides

#define snapver		rc1
%define rpmversion	4.12.0.2
%define srcver          %{rpmversion}%{?snapver:-%{snapver}}
%define libver		4.12
%define libmajor	3
%define librpmname      %mklibname rpm  %{libmajor}
%define librpmnamedevel %mklibname -d rpm
%define librpmsign      %mklibname rpmsign %{libmajor}
%define librpmbuild     %mklibname rpmbuild %{libmajor}

%define buildpython 1

%define rpmsetup_version 1.34

%define builddebug 0
%{?_with_debug:%define builddebug 1}

%{?_with_python:%define buildpython 1}
%{?_without_python:%define buildpython 0}

# disable plugins initially
%define buildplugins 1
%{?_with_plugins:%define buildplugins 1}

Summary:	The RPM package management system
Name:		rpm
Version:        %{rpmversion}
Release:	%mkrel %{?snapver:0.%{snapver}.}1
Group:		System/Configuration/Packaging
Source:		http://www.rpm.org/releases/rpm-%{libver}.x/rpm-%{srcver}.tar.bz2
# extracted from http://pkgs.fedoraproject.org/cgit/redhat-rpm-config.git/plain/macros:
Source1:	macros.filter

#
# Fedora patches
#
# Patches already upstream:
# Dont wait for transaction lock inside scriptlets (#1135596)
Patch100: rpm-4.12.0-tslock-nowait.patch
# find-debuginfo.sh fails on ELF with more than 256 notes
# http://www.rpm.org/ticket/887
Patch103: 0001-Fix-find-debuginfo.sh-for-ELF-with-file-warnings.patch
# Fix --excludedocs option (#1192625)
Patch104: rpm-4.12.0-exclude-doc.patch
# Pass _find_debuginfo_opts -g to eu-strip for executables (#1186563)
Patch105: rpm-4.12.0-eu-strip-g-option.patch
# Fix golang debuginfo packages
Patch106: rpm-4.12.0-golang-debuginfo.patch
Patch107: rpm-4.12.0-whatrecommends.patch
Patch108: rpm-4.12.0-gpg-passphrase1.patch
Patch109: rpm-4.12.0-gpg-passphrase2.patch

# (ngompa): Fix autopatch/autosetup issues (backport from Cauldron)
Patch116: rpm-4.13.0-autopatch-fix.patch
Patch117: rpm-4.13.0-autosetup-errors.patch
Patch118: rpm-4.13.0-fuzz-settings.patch

# These are not yet upstream
# Compressed debuginfo support (#833311)
Patch305: rpm-4.10.0-dwz-debuginfo.patch
# Minidebuginfo support (#834073)
Patch306: rpm-4.10.0-minidebuginfo.patch

# Temporary band-aid for rpm2cpio whining on payload size mismatch (rhbz#1142949)
Patch5: rpm-4.12.0-rpm2cpio-hack.patch

# - force /usr/lib/rpm/rambo/rpmrc instead of /usr/lib/rpm/<vendor>/rpmrc
# - read /usr/lib/rpm/rambo/rpmpopt (not only /usr/lib/rpm/rpmpopt)
# if we deprecated the use of rpm -ba , ...,  we can get rid of this patch
# Derived from mga5 patch
Patch64:    rpm-4.9.1.1-rambo-rpmrc-rpmpopt.patch

# In original rpm, -bb --short-circuit does not work and run all stage
# From popular request, we allow to do this
# http://qa.mandriva.com/show_bug.cgi?id=15896
Patch70:	rpm-4.9.1-bb-shortcircuit.patch

# don't conflict for doc files
# (to be able to install lib*-devel together with lib64*-devel even if they have conflicting manpages)
Patch83: rpm-4.12.0-no-doc-conflicts.patch

# Fix http://qa.mandriva.com/show_bug.cgi?id=19392
# (is this working??)
Patch84: rpm-4.4.2.2-rpmqv-ghost.patch

# Fix diff issue when buildroot contains some "//"
Patch111: rpm-check-file-trim-double-slash-in-buildroot.patch

# [Dec 2008] macrofiles from rpmrc does not overrides MACROFILES anymore
# Upstream 4.11 will have /usr/lib/rpm/macros.d:
Patch120: rpm-4.9.0-read-macros_d-dot-macros.patch

# (from Turbolinux) remove a wrong check in case %_topdir is /RPM (ie when it is short)
# Panu said: "To my knowledge this is a true technical limitation of the
# implementation: as long as debugedit can just overwrite data in the elf
# sections things keep relatively easy, but if dest_dir is longer than the
# original directory, debugedit would have to expand the whole elf file. Which
# might be technically possible but debugedit currently does not even try to."
Patch135: rpm-4.12.0-fix-debugedit.patch

# without this patch, "#%define foo bar" is surprisingly equivalent to "%define foo bar"
# with this patch, "#%define foo bar" is a fatal error
# Bug still valid => Send upstream for review.
Patch145: rpm-forbid-badly-commented-define-in-spec.patch

# cf http://wiki.mandriva.com/en/Rpm_filetriggers
Patch146: rpm-4.12.0-filetriggers.patch
Patch147: rpm-4.11.1-filetriggers-priority.patch
Patch148: rpm-4.11.1-filetriggers-warnings.patch

# Introduce (deprecated) %%apply_patches:
# (To be dropped once all pkgs are converted to %%autosetup / %%autopatch)
Patch157: rpm-4.10.1-introduce-_after_setup-which-is-called-after-setup.patch
Patch159: introduce-apply_patches-and-lua-var-patches_num.patch

#
# Merge mageia's perl.prov improvements back into upstream:
#
# ignore .pm files for perl provides
Patch160: ignore-non-perl-modules.diff
# making sure automatic provides & requires for perl package are using the new
# macro %%perl_convert_version:
Patch162: use_perl_convert_version.diff
# skip plain, regular comments:
Patch163: skip-plain-regular-comments.diff
# support for _ in perl module version:
Patch164: support-for-_-in-perl-module-version.diff

#
# Merge mageia's find-requires.sh improvements back into upstream:
#
# (tv) output perl-base requires instead of /usr/bin/perl with internal generator:
# (ngompa) This patch can be dropped once switched to rpm-md repos
Patch170: script-perl-base.diff
# (tv) do not emit requires for /bin/sh (required by glibc) or interpreters for which
# we have custom
# (ngompa) This patch can be dropped once switched to rpm-md repos
Patch172: script-filtering.diff
# (tv) "resolve" /bin/env foo interpreter to actual path, rather than generating
# dependencies on coreutils, should trim off ~800 dependencies more
Patch173: script-env.diff
# (tv) output pkgconfig requires instead of /usr/bin/pkgconfig.diff with internal generator:
# (ngompa) This patch can be dropped once switched to rpm-md repos
Patch174: pkgconfig.diff
# (tv) replace file deps by requires on packages:
# (ngompa) This patch can be dropped once switched to rpm-md repos
Patch176: script-no-file-deps.diff
# (pt) generate ELF provides for libraries, not only for executables
Patch180: elf_libs_req.diff 
Patch181: assumeexec.diff 
Patch1007: rpm-4.12.0-xz-support.patch

# Turbolinux patches
# Crusoe CPUs say that their CPU family is "5" but they have enough features for i686.
Patch2003: rpm-4.4.2.3-rc1-transmeta-crusoe-is-686.patch

Patch2006: rpm-4.10.0-setup-rubygems.patch

# (tv) fix tests:
Patch2100: rpm-4.11.1-fix-testsuite.diff
Patch2101: 0001-fix-testsuite-adjust-pkg-list.patch

Patch3000: mips_macros.patch
Patch3002: mips_define_isa_macros.patch
Patch3003: rpm_arm_mips_isa_macros.patch
Patch3004: rpm_add_armv5tl.patch

# (tv) Temporary Patch to provide support for updates (FC):
Patch3500: rpm-4.10.90-rpmlib-filesystem-check.patch

# Mageia patches that are easier to rediff on top of FC patches:
#---------------------------------------------------------------
# (tv) merge mga stuff from rpm-setup:
Patch4000: rpm-4.10.0-find-debuginfo__mga-cfg.diff
# (cg) fix debuginfo extraction. Sometimes, depending on local setup, the
# extraction of debuginfo can fail. This happens if you have a shared build dir
# which contains lots of subfolders for different packages (i.e. the default
# you would get if you rpm -i lots of srpms and build a whole bunch of them)
# This fix simply uses the real build dir passed in as an argument to the script
# rather than the top level %_builddir definition (aka $RPM_BUILD_DIR).
# (cg) This messes up the debuginfo packages themselves due to bad paths.
# I suspect the real problem lies in the debugedit binary which I will debug further.
# Leaving this here so I don't forget (aka it annoys tv enough to bug me if it's
# still here after any reasonable length of time!)
#Patch4007: rpm-4.11.1-fix-debuginfo-extraction.patch
# (lm) Don't uselessly bytecompile .py in docdir
Patch4008: rpm-4.11.1-dont-bytecompile-python-in-docdir.patch

Patch4009: rpm-4.11.2-double-separator-warning.patch
# from git:
Patch4020: pydoc.diff

# ovitters: make parseSpec work under Python3
Patch7001: rpm-4.11.2-py3-fix-import.patch

License:	GPLv2+
BuildRequires:	autoconf >= 2.69
BuildRequires:	zlib-devel
BuildRequires:  bzip2-devel
BuildRequires:	liblzma-devel >= 5.2.0
BuildRequires:	automake >= 1.14
BuildRequires:	elfutils-devel
BuildRequires:	sed >= 4.0.3
BuildRequires:	libbeecrypt-devel
BuildRequires:	ed, gettext-devel
BuildRequires:  libsqlite3-devel
BuildRequires:  db5.3-devel
BuildRequires:  neon-devel
BuildRequires:	popt-devel
BuildRequires:	nss-devel
BuildRequires:	magic-devel
BuildRequires:  readline-devel >= 6.3
BuildRequires:	ncurses-devel
BuildRequires:  openssl-devel
BuildRequires:  lua5.2-devel >= 5.2.4
BuildRequires:  libcap-devel
BuildRequires:  pkgconfig(libarchive)
# Needed for doc
BuildRequires:	texlive
%if %buildpython
BuildRequires:  python-devel
BuildRequires:  python3-devel
%endif
# for testsuite:
BuildRequires: eatmydata
BuildRequires: fakechroot

Requires:	bzip2
Requires:	xz
Requires:	cpio
Requires:	gawk
Requires:	glibc
Requires:	mktemp
Requires:	setup
# PCLinuxOS has rpm-VENDOR-setup split into two packages
Requires:	rpm-rambo-setup, rpm-pclinuxos-setup
Requires:	update-alternatives
Requires:	%librpmname = %version-%release
URL:            http://rpm.org/
%define         git_url        http://rpm.org/git/rpm.git
Requires(pre):		rpm-helper
Requires(pre):		coreutils
Requires(postun):	rpm-helper
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-buildroot

# (Conan Kudo): Not sure what this is for...
#Conflicts: jpackage-utils < 1:1.7.5-17
# fix for plugins conflict:
Conflicts: %{_lib}rpm3 < %{version}-%{release}

%description
RPM is a powerful command line driven package management system capable of
installing, uninstalling, verifying, querying, and updating software packages.
Each software package consists of an archive of files along with information
about the package like its version, a description, etc.

%package   -n %librpmbuild
Summary:   Libraries for building and signing RPM packages
Group:     System/Libraries

%description -n %librpmbuild
This package contains the RPM shared libraries for building and signing
packages.

%package  -n %librpmsign
Summary:  Libraries for building and signing RPM packages
Group:    System/Libraries

%description -n %librpmsign
This package contains the RPM shared libraries for building and signing
packages.

%package -n %librpmname
Summary:  Library used by rpm
Group:	  System/Libraries
Provides: librpm = %version-%release
# for fixed lua:
Requires:  %{mklibname lua 5.2} >= 5.2.4

%description -n %librpmname
RPM is a powerful command line driven package management system capable of
installing, uninstalling, verifying, querying, and updating software packages.
This package contains common files to all applications based on rpm.

%package -n %librpmnamedevel
Summary:	Development files for applications which will manipulate RPM packages
Group:		Development/Libraries
Requires:	rpm = %{version}-%{release}
Provides:	librpm-devel = %version-%release
Provides:   	rpm-devel = %version-%release
Requires:       %librpmname = %version-%release
Requires:       %librpmbuild = %version-%release
Requires:       %librpmsign = %version-%release

%description -n %librpmnamedevel
This package contains the RPM C library and header files.  These
development files will simplify the process of writing programs
which manipulate RPM packages and databases and are intended to make
it easier to create graphical package managers or any other tools
that need an intimate knowledge of RPM packages in order to function.

This package should be installed if you want to develop programs that
will manipulate RPM packages and databases.

%package build
Summary:	Scripts and executable programs used to build packages
Group:		System/Configuration/Packaging
Requires:	autoconf
Requires:	automake
Requires:	file
Requires:	gcc-c++
# We need cputoolize & amd64-* alias to x86_64-* in config.sub
Requires:	libtool-base
Requires:	patch
Requires:	make
Requires:	tar
Requires:	unzip
Requires:	elfutils
Requires:	rpm = %{version}-%{release}
Requires:	rpm-pclinuxos-setup-build %{?rpmsetup_version:>= %{rpmsetup_version}}
Requires:	%librpmbuild = %version-%release

%description build
This package contains scripts and executable programs that are used to
build packages using RPM.

%package sign
Summary: Package signing support
Group:   System/Base

%description sign
This package contains support for digitally signing RPM packages.

%if %buildpython
%package -n python2-rpm
Summary:	Python 2 bindings for apps which will manipulate RPM packages
Group:		Development/Python
Obsoletes:	python-rpm < %version-%release
Provides:	python-rpm = %version-%release
Requires:	python
Requires:	rpm = %{version}-%{release}
Obsoletes:	rpm-python < %version-%release
Provides:	rpm-python = %version-%release

%description -n python2-rpm
The rpm-python package contains a module which permits applications
written in the Python programming language to use the interface
supplied by RPM (RPM Package Manager) libraries.

This package should be installed if you want to develop Python 2
programs that will manipulate RPM packages and databases.

%package -n python3-rpm
Summary:	Python 3 bindings for apps which will manipulate RPM packages
Group:		Development/Python
Requires:	python3
Requires:	rpm = %{version}-%{release}
Obsoletes:	rpm-python3 < %version-%release
Provides:	rpm-python3 = %version-%release

%description -n python3-rpm
The rpm-python package contains a module which permits applications
written in the Python programming language to use the interface
supplied by RPM (RPM Package Manager) libraries.

This package should be installed if you want to develop Python 3
programs that will manipulate RPM packages and databases.
%endif

%prep
%setup -q -n %name-%srcver
%apply_patches

%build
%define _disable_ld_no_undefined 1
aclocal
automake-1.14 --add-missing
automake
autoreconf

%if %builddebug
RPM_OPT_FLAGS=-g
%endif
export CPPFLAGS="$CPPFLAGS `pkg-config --cflags nss`"
CFLAGS="$RPM_OPT_FLAGS -fPIC" CXXFLAGS="$RPM_OPT_FLAGS -fPIC" \
    %configure2_5x \
        --enable-nls \
        --enable-sqlite3 \
        --without-javaglue \
%if %builddebug
        --enable-debug \
%endif
        --with-external-db \
%if %buildpython
        --enable-python \
%else
        --without-python \
%endif
%if ! %buildplugins
        --disable-plugins \
%endif
        --with-glob \
        --without-selinux \
        --without-apidocs \
        --with-cap

make %{?_smp_mflags}
%if %buildpython
pushd python
%{__python2} setup.py build
%{__python3} setup.py build
popd
%endif

%install
rm -rf $RPM_BUILD_ROOT

make DESTDIR=%buildroot install

%if %buildpython
# We need to build with --enable-python for the self-test suite, but we
# actually package the bindings built with setup.py (#531543#c26)
rm -rf $RPM_BUILD_ROOT/%{python_sitearch}
pushd python
%{__python2} setup.py install --skip-build --root $RPM_BUILD_ROOT
%{__python3} setup.py install --skip-build --root $RPM_BUILD_ROOT
popd
%endif

find $RPM_BUILD_ROOT -name "*.la"|xargs rm -f

# Save list of packages through cron
mkdir -p ${RPM_BUILD_ROOT}/etc/cron.daily
install -m 755 scripts/rpm.daily ${RPM_BUILD_ROOT}/etc/cron.daily/rpm

mkdir -p ${RPM_BUILD_ROOT}/etc/logrotate.d
install -m 644 scripts/rpm.log ${RPM_BUILD_ROOT}/etc/logrotate.d/rpm

mkdir -p $RPM_BUILD_ROOT/var/lib/rpm
for dbi in \
	Basenames Conflictname Dirnames Group Installtid Name Providename \
	Provideversion Removetid Requirename Requireversion Triggername \
	Obsoletename Packages Sha1header Sigmd5 __db.001 __db.002 \
	__db.003 __db.004 __db.005 __db.006 __db.007 __db.008 __db.009
do
    touch $RPM_BUILD_ROOT/var/lib/rpm/$dbi
done

test -d doc-copy || mkdir doc-copy
rm -rf doc-copy/*
ln -f doc/manual/* doc-copy/
rm -f doc-copy/Makefile*

mkdir -p $RPM_BUILD_ROOT/var/spool/repackage

mkdir -p %buildroot%rpmdir/macros.d
install %SOURCE1 %buildroot%rpmdir/macros.d
mkdir -p %buildroot%_sysconfdir/rpm/macros.d
cat > %buildroot%_sysconfdir/rpm/macros <<EOF
# Put your own system macros here
# usually contains 

# Set this one according your locales
# %%_install_langs

EOF

%{rpmdir}/%{_host_vendor}/find-lang.pl $RPM_BUILD_ROOT %{name}

%check
eatmydata make check
[ "$(ls -A tests/rpmtests.dir)" ] && cat tests/rpmtests.log

%pre
/usr/share/rpm-helper/add-user rpm $1 rpm /var/lib/rpm /bin/false

rm -rf /usr/lib/rpm/*-mandrake-*
rm -rf /usr/lib/rpm/*-%{_real_vendor}-*


%post
# nuke __db.00? when updating to this rpm
rm -f /var/lib/rpm/__db.00?

if [ ! -f /var/lib/rpm/Packages ]; then
    /bin/rpm --initdb
fi
# To ensure that the database is sane
/bin/rpm --rebuilddb


%postun
/usr/share/rpm-helper/del-user rpm $1 rpm

%define	rpmattr		%attr(0755, rpm, rpm)

%files -f %{name}.lang
%doc GROUPS CHANGES doc/manual/[a-z]*
%attr(0755,rpm,rpm) /bin/rpm
%attr(0755, rpm, rpm) %{_bindir}/rpm2cpio
%attr(0755, rpm, rpm) %{_bindir}/rpm2archive
%attr(0755, rpm, rpm) %{_bindir}/gendiff
%attr(0755, rpm, rpm) %{_bindir}/rpmdb
%attr(0755, rpm, rpm) %{_bindir}/rpmkeys
%attr(0755, rpm, rpm) %{_bindir}/rpmgraph
%{_bindir}/rpmquery
%{_bindir}/rpmverify
%if %buildplugins
%{_libdir}/rpm-plugins
%endif

%dir %{_localstatedir}/spool/repackage
%dir %{rpmdir}
%dir /etc/rpm
%config(noreplace) /etc/rpm/macros
%dir /etc/rpm/macros.d
%attr(0755, rpm, rpm) %{rpmdir}/config.guess
%attr(0755, rpm, rpm) %{rpmdir}/config.sub
%attr(0755, rpm, rpm) %{rpmdir}/rpmdb_*
%attr(0644, rpm, rpm) %{rpmdir}/macros
%rpmdir/macros.d
%attr(0755, rpm, rpm) %{rpmdir}/mkinstalldirs
%attr(0755, rpm, rpm) %{rpmdir}/rpm.*
%attr(0644, rpm, rpm) %{rpmdir}/rpmpopt*
%attr(0644, rpm, rpm) %{rpmdir}/rpmrc
%attr(0755, rpm, rpm) %{rpmdir}/elfdeps
%attr(0755, rpm, rpm) %{rpmdir}/script.req
%exclude %{rpmdir}/tcl.req

%rpmattr	%{rpmdir}/rpm2cpio.sh
%rpmattr	%{rpmdir}/tgpg

%dir %attr(   -, rpm, rpm) %{rpmdir}/fileattrs
%attr(0644, rpm, rpm) %{rpmdir}/fileattrs/*.attr

%dir %attr(   -, rpm, rpm) %{rpmdir}/platform/
%exclude %{rpmdir}/platform/m68k-linux/macros
%ifarch %{ix86} x86_64
%attr(   -, rpm, rpm) %{rpmdir}/platform/i*86-*
%attr(   -, rpm, rpm) %{rpmdir}/platform/athlon-*
%attr(   -, rpm, rpm) %{rpmdir}/platform/pentium*-*
%attr(   -, rpm, rpm) %{rpmdir}/platform/geode-*
%else
%exclude %{rpmdir}/platform/i*86-%{_os}/macros
%exclude %{rpmdir}/platform/athlon-%{_os}/macros
%exclude %{rpmdir}/platform/pentium*-%{_os}/macros
%exclude %{rpmdir}/platform/geode-%{_os}/macros
%endif
%ifarch x86_64
%attr(   -, rpm, rpm) %{rpmdir}/platform/amd64-*
%attr(   -, rpm, rpm) %{rpmdir}/platform/x86_64-*
%attr(   -, rpm, rpm) %{rpmdir}/platform/ia32e-*
%else
%exclude %{rpmdir}/platform/amd64-%{_os}/macros
%exclude %{rpmdir}/platform/ia32e-%{_os}/macros
%exclude %{rpmdir}/platform/x86_64-%{_os}/macros
%endif
%ifarch %arm
%attr(   -, rpm, rpm) %{rpmdir}/platform/arm*
%attr(   -, rpm, rpm) %{rpmdir}/platform/aarch64*/macros
%else
%exclude %{rpmdir}/platform/arm*/macros
%exclude %{rpmdir}/platform/aarch64*/macros
%endif
%ifarch %mips
%attr(   -, rpm, rpm) %{rpmdir}/platform/mips*
%endif
%attr(   -, rpm, rpm) %{rpmdir}/platform/noarch*
# new in 4.10.0:
%exclude %{rpmdir}/platform/alpha*-%{_os}/macros
%exclude %{rpmdir}/platform/sparc*-%{_os}/macros
%exclude %{rpmdir}/platform/ia64*-%{_os}/macros
%exclude %{rpmdir}/platform/m68k*-%{_os}/macros
%exclude %{rpmdir}/platform/ppc*-%{_os}/macros
%exclude %{rpmdir}/platform/s390*-%{_os}/macros
%exclude %{rpmdir}/platform/sh*-%{_os}/macros



%{_mandir}/man[18]/*.[18]*
%lang(pl) %{_mandir}/pl/man[18]/*.[18]*
%lang(ru) %{_mandir}/ru/man[18]/*.[18]*
%lang(ja) %{_mandir}/ja/man[18]/*.[18]*
%lang(sk) %{_mandir}/sk/man[18]/*.[18]*
%lang(fr) %{_mandir}/fr/man[18]/*.[18]*
%lang(ko) %{_mandir}/ko/man[18]/*.[18]*

%config(noreplace,missingok)	/etc/cron.daily/rpm
%config(noreplace,missingok)	/etc/logrotate.d/rpm

%attr(0755, rpm, rpm)	%dir %_localstatedir/lib/rpm

%define	rpmdbattr %attr(0644, rpm, rpm) %verify(not md5 size mtime) %ghost %config(missingok,noreplace)

%rpmdbattr	/var/lib/rpm/Basenames
%rpmdbattr	/var/lib/rpm/Conflictname
%rpmdbattr	/var/lib/rpm/__db.0*
%rpmdbattr	/var/lib/rpm/Dirnames
%rpmdbattr	/var/lib/rpm/Group
%rpmdbattr	/var/lib/rpm/Installtid
%rpmdbattr	/var/lib/rpm/Name
%rpmdbattr	/var/lib/rpm/Obsoletename
%rpmdbattr	/var/lib/rpm/Packages
%rpmdbattr	/var/lib/rpm/Providename
%rpmdbattr	/var/lib/rpm/Provideversion
%rpmdbattr	/var/lib/rpm/Removetid
%rpmdbattr	/var/lib/rpm/Requirename
%rpmdbattr	/var/lib/rpm/Requireversion
%rpmdbattr	/var/lib/rpm/Sha1header
%rpmdbattr	/var/lib/rpm/Sigmd5
%rpmdbattr	/var/lib/rpm/Triggername

%files build
%doc CHANGES
%doc doc-copy/*
%rpmattr	%{_bindir}/rpmbuild
%rpmattr        %{_bindir}/rpmspec
%rpmattr	%{_prefix}/lib/rpm/brp-*
%rpmattr	%{_prefix}/lib/rpm/check-files
%rpmattr	%{_prefix}/lib/rpm/debugedit
%rpmattr	%{_prefix}/lib/rpm/*.prov 
%rpmattr	%{_prefix}/lib/rpm/find-debuginfo.sh
%rpmattr	%{_prefix}/lib/rpm/find-lang.sh
%rpmattr	%{_prefix}/lib/rpm/find-provides
%rpmattr	%{_prefix}/lib/rpm/find-requires
%rpmattr	%{_prefix}/lib/rpm/perldeps.pl
%rpmattr	%{_prefix}/lib/rpm/perl.req

%rpmattr	%{_prefix}/lib/rpm/check-buildroot
%rpmattr	%{_prefix}/lib/rpm/check-prereqs
%rpmattr	%{_prefix}/lib/rpm/check-rpaths
%rpmattr	%{_prefix}/lib/rpm/check-rpaths-worker
%rpmattr	%{_prefix}/lib/rpm/libtooldeps.sh
%rpmattr	%{_prefix}/lib/rpm/macros.perl
%rpmattr	%{_prefix}/lib/rpm/macros.php
%rpmattr	%{_prefix}/lib/rpm/macros.python
%rpmattr	%{_prefix}/lib/rpm/mono-find-provides
%rpmattr	%{_prefix}/lib/rpm/mono-find-requires
%rpmattr	%{_prefix}/lib/rpm/ocaml-find-provides.sh
%rpmattr	%{_prefix}/lib/rpm/ocaml-find-requires.sh
%rpmattr	%{_prefix}/lib/rpm/osgideps.pl
%rpmattr	%{_prefix}/lib/rpm/pkgconfigdeps.sh

%rpmattr	%{_prefix}/lib/rpm/rpmdeps
%rpmattr        %{_prefix}/lib/rpm/pythondeps.sh


%{_mandir}/man8/rpmbuild.8*
%{_mandir}/man8/rpmdeps.8*

%if %buildpython
%files -n python2-rpm
%{python2_sitearch}/rpm
%{python2_sitearch}/rpm_python-%{version}-py2.7.egg-info

%files -n python3-rpm
%defattr(-,root,root)
%{python3_sitearch}/rpm
%{python3_sitearch}/rpm_python-%{version}-py%{python3_version}.egg-info
%endif

%files -n %librpmname
%{_libdir}/librpm.so.%{libmajor}*
%{_libdir}/librpmio.so.%{libmajor}*

%files -n %librpmbuild
%{_libdir}/librpmbuild.so.%{libmajor}*

%files -n %librpmsign
%{_libdir}/librpmsign.so.%{libmajor}*

%files sign
%{_bindir}/rpmsign
%{_mandir}/man8/rpmsign.8*

%files -n %librpmnamedevel
%{_includedir}/rpm
%{_libdir}/librpm.so
%{_libdir}/librpmio.so
%{_libdir}/librpmbuild.so
%{_libdir}/librpmsign.so
%{_libdir}/pkgconfig/rpm.pc


%changelog
* Fri Dec 23 2016 Conan Kudo <Conan Kudo> 4.12.0.2-1pclos2017
- Update to rpm 4.12.0.2
- Based on rpm-4.12.0.2-1.7.mga5 from Mageia

* Mon Mar 28 2016 Conan Kudo <Conan Kudo> 4.12.0.1-1pclos2016
- Update to rpm 4.12.0.1
- Based on rpm-4.12.0.1-20.4.mga5 from Mageia

* Wed Oct 02 2013 pinoc <vogtpet at gmail.com> 4.8.1-7pclos2013
- fix compiler, cpu, arch settings for 32bit in /usr/lib/rpm/macros

* Mon Aug 19 2013 billybot <billybot> 4.8.1-6pclos2013
- update python 2.7

* Mon Sep 26 2011 Texstar <texstar at gmail.com> 4.8.1-5pclos2011
- add custom macro for patch fuzz=2 like it was before

* Fri Jun 10 2011 Texstar <texstar at gmail.com> 4.8.1-4pclos2011
- update

* Fri Jun 03 2011 Texstar <texstar at gmail.com> 4.8.1-3pclos2011
- rebuild

* Fri Jun 03 2011 Texstar <texstar at gmail.com> 4.8.1-2pclos2011
- rebuild

* Thu Jun 02 2011 Texstar <texstar at gmail.com> 4.8.1-1pclos2011
- update for pclinuxos 2011 release
